# App stack404

App stack404 partially automates the process of finding dead external links in posts on [Stack Exchange Q&A](https://stackexchange.com/sites#name) sites.

Stack404 works on batches of post data gathered manually with the [Stack Exchange Data Explorer](https://data.stackexchange.com/help). From this data it extracts the links with external destinations and determines the status of the link destinations. An interactive graphical summary is displayed in a web browser.

<img alt="An enticing preview of stack404" src="readme_category_viewer.png">

### Dependencies
* Linux.
* Firefox and Chrome.
* Go.

### Install
From a terminal, with the current working directory as the directory where you wish to install, execute the command:
```sh
git clone https://gitlab.com/fospathi/stack404.git/
```

Now change directory into the correct directory to follow the how-to guide below:
```sh
cd stack404
```

### How-to

1. Generate a batch of post data from which to extract links:
   1. Visit this [query runner](https://data.stackexchange.com/stackoverflow/query/1159644/all-posts-of-type-1-2-or-3-contained-in-a-given-batch-of-200-ids).
   2. Switch to your desired site to check using the site switcher at the **bottom** of the page: <div align="center"><img alt="Helpful pic of the  switch sites box" src="readme_switch_sites.png"></div>
   3. Enter a batch number, it should be at least 1. The maximum batch number depends on how many posts the chosen domain has. You can guess it by visiting the most recent post and looking for the post's ID number in the web address and dividing it by the number of posts in a batch, which is 200 by default. <div align="center"><img alt="Helpful pic of the enter parameters box" src="readme_enter_parameters.png"></div>
   4. Tap the Run Query button. You may need to solve a CAPTCHA and then press the Run Query button again. Now download the data by clicking the Download CSV link at the top right of the newly appeared data table. Save the file using its default name of QueryResults.csv in this directory alongside the main.go file.<div align="center"><img alt="Helpful pic of the download csv link" src="readme_download_csv.png"></div>
   
2. Run the app from a terminal: with the current working directory as this directory execute the command: 
    ```sh
    go run .
    ```
3. In a browser visit the web address which will be printed on the terminal.

### Tips
* Soft 404s: check the screenshots for signs of 404s, page not founds, for sale ads, and content not related to the URL.
* :mag: Moved: to find a moved site's new location try searching on a search engine using the URL or if that doesn't work you can search for it using snippets of the site's content or title which you can usually find on the Internet Archive's [Wayback Machine](https://archive.org/web/).
* :crystal_ball: Vanished: if the site has permanently vanished then as a last resort you can edit the link destination to use its Internet Archive page.
* :pick: Veins: when you find one dead link destination you might have struck it rich! Just enter the dead URL in the Q&A site's search box at the top to find its other occurrences.
* False positives: use your common sense to ignore any URLs that are only meant as example URLs and to ignore non-URL phrases that have the syntax of a URL.

### Development status

App stack404 is unstable; expect breaking changes with every commit.

## License

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://fospathi.inertialframe.space">
    <span property="dct:title">Christian Stewart</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">stack404</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="GB" about="https://fospathi.inertialframe.space">
  United Kingdom</span>.
</p>
