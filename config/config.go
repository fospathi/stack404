package config

const (
	// QueryResultsPath is where to extract the links from and is a CSV format file.
	QueryResultsPath = "QueryResults.csv"

	// FilesPath lets the server know where the files to be served are relative to the app's root directory
	// which contains the main.go file.
	FilesPath = "./flutter/stack_404/build/web"

	// Timeout is the maximum number of seconds to wait before giving up on a site.
	Timeout = 30

	// Pad is the number of padding chars included in the context either side of the url.
	Pad = 50
)

// Serve the links status messages on a websocket on this address.
const (
	SrvAddr = "localhost:8080"
	WSPath  = "/ws"
)

// The dimensions of the captures.
const (
	PicWidth  = 1366
	PicHeight = 768
)
