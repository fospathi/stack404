/*
Package firecrow uses firefox and google-chrome to make website screenshots in parallel.

Serial instances of firefox are run in parallel with serial instances of google-chrome. Being of independent
architectures they do not interfer with each other as do multiple parallel invocations of the same browser
seem to do.
*/
package firecrow

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"image"
	"image/png"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/fospathi/stack404/link"
	"gitlab.com/fospathi/watchable"
)

// Used to make sure the pseudo random number generator is seeded just once.
var randOnce sync.Once

const (
	chrome  = "chrome"
	firefox = "firefox"
)

// A Screenshot of a website.
type Screenshot struct {
	Bytes   []byte
	Ok      bool
	Timeout bool // True if timed out or the capture function's argument context was cancelled.
}

// New constructs a new concurrency safe screenshot function which captures PNG format images of websites in
// parallel.
//
// The arguments to New are the dimensions, in pixels, of the captures.
//
// It is normally sufficient to construct one screenshot function: using multiple screenshot functions in
// parallel is likely to be counterproductive.
//
// A screenshot function runs consecutive serial instances of firefox in parallel with consecutive serial
// instances of google-chrome; both firefox and google-chrome need to be available.
//
// To permanently cancel a screenshot function and end its goroutines use the cancel function returned by New.
func New(width, height int) (capture func(context.Context, link.L) (Screenshot, error), cancel func()) {
	const filenameLen = 32 // This many random chars long plus the prefix and suffix.
	size := strconv.Itoa(width) + "," + strconv.Itoa(height)
	oncePer10 := watchable.NewOnceEvery(10)

	randOnce.Do(func() {
		rand.Seed(time.Now().UnixNano())
	})

	type screenshot struct {
		Screenshot
		err error
	}
	type resource struct {
		ctx context.Context
		l   link.L
		sc  chan screenshot
	}

	cmdParams := func(r resource) (res resource, name string, url string) {
		res = r
		name = "tmp_" + randAlpha(filenameLen) + ".png"
		url = link.Schemify(r.l.URL())
		return
	}

	cCmd := func(r resource, name string, url string) (*exec.Cmd, resource, string, string, string) {
		return exec.CommandContext(r.ctx,
				"google-chrome",
				"--headless",
				"--disable-gpu",
				"--screenshot="+name,
				"--window-size="+size,
				url),
			r,
			name,
			url,
			chrome
	}

	fCmd := func(r resource, name string, url string) (*exec.Cmd, resource, string, string, string) {
		return exec.CommandContext(r.ctx,
				"firefox",
				"-profile", "firefoxprofile",
				"--screenshot="+name,
				"--window-size="+size,
				url),
			r,
			name,
			url,
			firefox
	}

	var execCmd func(*exec.Cmd, resource, string, string, string) screenshot
	execCmd = func(cmd *exec.Cmd, r resource, name string, url string, browser string) screenshot {
		var (
			vb  []byte
			err error
		)
		if err = cmd.Run(); err != nil {
			if r.ctx.Err() == context.DeadlineExceeded || r.ctx.Err() == context.Canceled {
				return screenshot{Screenshot{Timeout: true}, nil}
			}
			return screenshot{Screenshot{}, wrap(err, "cmd.Run")}
		}
		if vb, err = ioutil.ReadFile(name); err != nil {
			// Try with chrome instead.
			if firefox == browser {
				return execCmd(cCmd(r, name, url))
			}
			return screenshot{Screenshot{}, wrap(err, browser+": "+"ioutil.ReadFile")}
		}
		if err = os.Remove(name); err != nil {
			return screenshot{Screenshot{}, wrap(err, "os.Remove")}
		}
		if vb, err = cropPNG(vb, width, height); err != nil {
			return screenshot{Screenshot{}, wrap(err, "cropPNG")}
		}
		return screenshot{Screenshot{Bytes: vb, Ok: true}, nil}
	}

	cc := make(chan resource)
	fc := make(chan resource)
	done := make(chan struct{})
	var mux sync.Mutex

	go func() {
		for {
			select {
			case r := <-fc:
				var err error
				oncePer10.SyncDo(func() {
					err = ResetFire()
				})
				if err != nil {
					r.sc <- screenshot{err: err}
				} else {
					resetFireMutex.RLock()
					r.sc <- execCmd(fCmd(cmdParams(r)))
					resetFireMutex.RUnlock()
				}
			case <-done:
				return
			}
		}
	}()

	go func() {
		for {
			select {
			case r := <-cc:
				r.sc <- execCmd(cCmd(cmdParams(r)))
			case <-done:
				return
			}
		}
	}()

	capture = func(ctx context.Context, l link.L) (Screenshot, error) {
		sc := make(chan screenshot)
		select {
		case fc <- resource{ctx, l, sc}:
			s := <-sc
			return s.Screenshot, s.err
		case cc <- resource{ctx, l, sc}:
			s := <-sc
			return s.Screenshot, s.err
		case <-done:
			return Screenshot{}, nil
		}
	}
	cancel = func() {
		mux.Lock()
		defer mux.Unlock()
		select {
		case <-done:
		default:
			close(done)
		}
	}
	return
}

const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randAlpha(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

// Crop a PNG image to a maximum width and height.
func cropPNG(vb []byte, width int, height int) ([]byte, error) {
	img, format, err := image.Decode(bytes.NewReader(vb))
	if err != nil {
		return nil, err
	}
	if "png" != strings.ToLower(format) {
		return nil, fmt.Errorf("image format: got %v, want png", format)
	}

	if h, w := img.Bounds().Dy(), img.Bounds().Dx(); h > height || w > width {
		if h > height {
			h = height
		}
		if w > width {
			w = width
		}
		subImager, ok := img.(interface {
			SubImage(image.Rectangle) image.Image
		})
		if !ok {
			// Warning: check that package "image/png" is imported: that library must be initialised for a PNG
			// format image to be recognised.
			return nil, errors.New("type assertion failure")
		}
		img = subImager.SubImage(image.Rectangle{
			Min: img.Bounds().Min,
			Max: img.Bounds().Min.Add(image.Point{X: w, Y: h}),
		})

		var b bytes.Buffer
		w := bufio.NewWriter(&b)
		err = png.Encode(w, img)
		if err != nil {
			return nil, err
		}
		w.Flush()
		return b.Bytes(), nil
	}

	return vb, nil
}

// Wrap the argument non-nil error in a firecrow error.
func wrap(err error, info string) *Error {
	if len(info) > 0 {
		return &Error{fmt.Errorf("%v: %w", info, err)}
	}
	return &Error{err}
}

// An Error indicates that something happened when trying to make a site screenshot with firecrow.
type Error struct {
	err error
}

func (e *Error) Error() string {
	return fmt.Sprintf("firecrow error: %v", e.err)
}

func (e *Error) Unwrap() error {
	return e.err
}
