package firecrow

import (
	"fmt"
	"io/ioutil"
	"os"
	"sync"

	"path/filepath"
)

// A global mutex to prevent parallel deletion attempts.
var resetFireMutex = sync.RWMutex{}

// ResetFire deletes the contents of the firefoxprofile directory.
//
// Leaves the .gitignore file.
//
// Expects certain directories to be present:
// * the working directory is stack404
// * a child directory is firefoxprofile
func ResetFire() error {
	resetFireMutex.Lock()
	defer resetFireMutex.Unlock()

	const wdn = "stack404"
	const pdn = "firefoxprofile"
	wd, err := os.Getwd()
	if err != nil {
		return wrap(err, "os.Getwd")
	}
	if dn := filepath.Base(wd); dn != wdn {
		return wrap(fmt.Errorf("working directory name: expected %v, got %v", wdn, dn), "")
	}
	pd := filepath.Join(wd, pdn)
	if _, err = os.Stat(pd); err != nil {
		return wrap(err, fmt.Sprintf("firefox profile directory: expected %v", pd))
	}

	files, err := ioutil.ReadDir(pd)
	if err != nil {
		return wrap(err, "ioutil.ReadDir")
	}
	for _, f := range files {
		fn := f.Name()
		if fn == ".gitignore" {
			continue // Leave the .gitignore file.
		} else if f.IsDir() {
			if err = os.RemoveAll(filepath.Join(pd, fn)); err != nil {
				return wrap(err, "os.RemoveAll")
			}
		} else {
			if err = os.Remove(filepath.Join(pd, fn)); err != nil {
				return wrap(err, "os.Remove")
			}
		}
	}

	return nil
}
