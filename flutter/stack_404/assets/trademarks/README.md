# Trademarks

The icons/logos in this directory are trademarks of other entities.

<a href="https://stackoverflow.com/legal/trademark-guidance">
  Stack Overflow:
  <img src="so.png" alt="Stack Overflow" style="width: 3em;"/>
</a>
