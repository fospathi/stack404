import 'package:flutter/material.dart';

import 'src/event.dart';
import 'src/widget/stack_404/bloc.dart';
import 'src/widget/stack_404/widget.dart';

void main() {
  final stack404 = Stack404Bloc();
  stack404.onLoadingLinkStatus = () {
    stack404.close();
  };
  runApp(stack404.se404BlocBuilder);
  stack404.add(Stack404Started());
}
