import 'dart:typed_data';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:web_socket_channel/html.dart';

import 'config/config.dart';
import 'protobuf/link_status/link_status.pb.dart' as pb;

class LinkStatus extends Equatable {
  LinkStatus.dummyStop(final int code)
      : ancestor = '',
        url = '',
        text = '',
        seSite = '',
        postUrl = '',
        postId = 0,
        postType = 0,
        responseStatusCode = code,
        codeOk = false,
        codeTimeout = false,
        screenshot = Image.memory(Uint8List.fromList([])),
        screenshotOk = false,
        screenshotTimeout = false,
        n = _linkStatusCounter++,
        isADummyStop = true;

  LinkStatus.fromProtobuf(final pb.Status status, {final complete: false})
      : ancestor = status.link.ancestor,
        url = status.link.url,
        text = status.link.text,
        seSite = status.link.seSite,
        postUrl = makePostUrl(status),
        postId = status.link.postId.toInt(),
        postType = status.link.postType.toInt(),
        responseStatusCode = status.responseStatusCode.toInt(),
        codeOk = status.codeOk,
        codeTimeout = status.codeTimeout,
        screenshot = Image.memory(Uint8List.fromList(status.screenshot)),
        screenshotOk = status.screenshotOk,
        screenshotTimeout = status.screenshotTimeout,
        n = _linkStatusCounter++,
        isADummyStop = complete;

  final String ancestor;
  final String url;
  final String text;
  final String seSite;
  final String postUrl;
  final int postId;
  final int postType;
  final int responseStatusCode;
  final bool codeOk;
  final bool codeTimeout;
  final Image screenshot;
  final bool screenshotOk;
  final bool screenshotTimeout;
  final int n; // A unique id for each link status received.

  // A true value indicates that the previous link status, if any, was the last
  // and that this is a dummy value to be ignored except for triggering
  // any finalisation and tidy up actions.
  final bool isADummyStop;

  static int _linkStatusCounter = 0;

  String get archiveUrl => 'https://web.archive.org/web/*/$url';

  bool get isQuestion => 1 == postType ? true : false;

  bool get deadlineExceeded => codeTimeout || screenshotTimeout;

  List<Object> get props =>
      [n, ancestor, responseStatusCode, text, url, isADummyStop];

  String toString() {
    return 'Instance of `LinkStatus`\n' +
        'status code: $responseStatusCode\n' +
        'url: $url\n' +
        'text: $text\n';
  }
}

String makePostUrl(final pb.Status status) {
  // q => question type post and a => answer or wiki type post
  final String segment = (1 == status.link.postType.toInt()) ? 'q' : 'a';
  final String site = status.link.seSite;
  final String slash = site.endsWith("/") ? "" : "/";
  return '$site$slash$segment/${status.link.postId}';
}

class LinkStatusProvider {
  // Connect to the server and retrieve all the link statuses.
  Stream<LinkStatus> readLinkStatus() {
    // The server closes this channel when all the link statuses have been sent.
    final channel = HtmlWebSocketChannel.connect('ws://$srvAddr$srvPath');

    Stream<LinkStatus> linkStatus(final Stream source) async* {
      await for (final message in source) {
        await Future.delayed(Duration(milliseconds: 500));
        yield LinkStatus.fromProtobuf(pb.Status.fromBuffer(message));
      }
      yield LinkStatus.dummyStop(0);
    }

    return linkStatus(channel.stream);
  }
}
