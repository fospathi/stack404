import 'package:equatable/equatable.dart';

import 'data.dart';

// A change in the top level app condition.
abstract class ConditionChanged extends Equatable {
  List<Object> get props => [];
}

class Stack404Started extends ConditionChanged {}

class SE404LinkStatusLoading extends ConditionChanged {}

class LinkStatusReceived {
  LinkStatusReceived(this.linkStatus);

  final LinkStatus linkStatus;

  Category get category {
    if (code >= 200) {
      if (code >= 300) {
        if (code >= 400) {
          if (code >= 500) {
            if (code >= 600) {
              return Category.codeXxx;
            }
            return Category.code5xx;
          }
          return Category.code4xx;
        }
        return Category.code3xx;
      }
      return Category.code2xx;
    }
    return Category.codeXxx;
  }

  int get code => linkStatus.responseStatusCode;
}

enum Category {
  code5xx,
  code4xx,
  code3xx,
  code2xx,
  codeXxx, // A catchall category for codes which don't belong to a more specific category.
}
