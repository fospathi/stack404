///
//  Generated code. Do not modify.
//  source: link_status.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

class Status_Link extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Status.Link', package: const $pb.PackageName('proto.link_status'), createEmptyInstance: create)
    ..aOS(1, 'ancestor')
    ..aOS(2, 'url')
    ..aOS(3, 'text')
    ..aOS(4, 'seSite')
    ..aInt64(5, 'postId')
    ..aInt64(6, 'postType')
    ..hasRequiredFields = false
  ;

  Status_Link._() : super();
  factory Status_Link() => create();
  factory Status_Link.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Status_Link.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Status_Link clone() => Status_Link()..mergeFromMessage(this);
  Status_Link copyWith(void Function(Status_Link) updates) => super.copyWith((message) => updates(message as Status_Link));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Status_Link create() => Status_Link._();
  Status_Link createEmptyInstance() => create();
  static $pb.PbList<Status_Link> createRepeated() => $pb.PbList<Status_Link>();
  @$core.pragma('dart2js:noInline')
  static Status_Link getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Status_Link>(create);
  static Status_Link _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get ancestor => $_getSZ(0);
  @$pb.TagNumber(1)
  set ancestor($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAncestor() => $_has(0);
  @$pb.TagNumber(1)
  void clearAncestor() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get url => $_getSZ(1);
  @$pb.TagNumber(2)
  set url($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasUrl() => $_has(1);
  @$pb.TagNumber(2)
  void clearUrl() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get text => $_getSZ(2);
  @$pb.TagNumber(3)
  set text($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasText() => $_has(2);
  @$pb.TagNumber(3)
  void clearText() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get seSite => $_getSZ(3);
  @$pb.TagNumber(4)
  set seSite($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasSeSite() => $_has(3);
  @$pb.TagNumber(4)
  void clearSeSite() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get postId => $_getI64(4);
  @$pb.TagNumber(5)
  set postId($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPostId() => $_has(4);
  @$pb.TagNumber(5)
  void clearPostId() => clearField(5);

  @$pb.TagNumber(6)
  $fixnum.Int64 get postType => $_getI64(5);
  @$pb.TagNumber(6)
  set postType($fixnum.Int64 v) { $_setInt64(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasPostType() => $_has(5);
  @$pb.TagNumber(6)
  void clearPostType() => clearField(6);
}

class Status extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Status', package: const $pb.PackageName('proto.link_status'), createEmptyInstance: create)
    ..aOM<Status_Link>(1, 'link', subBuilder: Status_Link.create)
    ..aInt64(2, 'responseStatusCode')
    ..aOB(3, 'codeOk')
    ..aOB(4, 'codeTimeout')
    ..a<$core.List<$core.int>>(5, 'screenshot', $pb.PbFieldType.OY)
    ..aOB(6, 'screenshotOk')
    ..aOB(7, 'screenshotTimeout')
    ..hasRequiredFields = false
  ;

  Status._() : super();
  factory Status() => create();
  factory Status.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Status.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Status clone() => Status()..mergeFromMessage(this);
  Status copyWith(void Function(Status) updates) => super.copyWith((message) => updates(message as Status));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Status create() => Status._();
  Status createEmptyInstance() => create();
  static $pb.PbList<Status> createRepeated() => $pb.PbList<Status>();
  @$core.pragma('dart2js:noInline')
  static Status getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Status>(create);
  static Status _defaultInstance;

  @$pb.TagNumber(1)
  Status_Link get link => $_getN(0);
  @$pb.TagNumber(1)
  set link(Status_Link v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasLink() => $_has(0);
  @$pb.TagNumber(1)
  void clearLink() => clearField(1);
  @$pb.TagNumber(1)
  Status_Link ensureLink() => $_ensure(0);

  @$pb.TagNumber(2)
  $fixnum.Int64 get responseStatusCode => $_getI64(1);
  @$pb.TagNumber(2)
  set responseStatusCode($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasResponseStatusCode() => $_has(1);
  @$pb.TagNumber(2)
  void clearResponseStatusCode() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get codeOk => $_getBF(2);
  @$pb.TagNumber(3)
  set codeOk($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCodeOk() => $_has(2);
  @$pb.TagNumber(3)
  void clearCodeOk() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get codeTimeout => $_getBF(3);
  @$pb.TagNumber(4)
  set codeTimeout($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCodeTimeout() => $_has(3);
  @$pb.TagNumber(4)
  void clearCodeTimeout() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<$core.int> get screenshot => $_getN(4);
  @$pb.TagNumber(5)
  set screenshot($core.List<$core.int> v) { $_setBytes(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasScreenshot() => $_has(4);
  @$pb.TagNumber(5)
  void clearScreenshot() => clearField(5);

  @$pb.TagNumber(6)
  $core.bool get screenshotOk => $_getBF(5);
  @$pb.TagNumber(6)
  set screenshotOk($core.bool v) { $_setBool(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasScreenshotOk() => $_has(5);
  @$pb.TagNumber(6)
  void clearScreenshotOk() => clearField(6);

  @$pb.TagNumber(7)
  $core.bool get screenshotTimeout => $_getBF(6);
  @$pb.TagNumber(7)
  set screenshotTimeout($core.bool v) { $_setBool(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasScreenshotTimeout() => $_has(6);
  @$pb.TagNumber(7)
  void clearScreenshotTimeout() => clearField(7);
}

