///
//  Generated code. Do not modify.
//  source: link_status.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const Status$json = const {
  '1': 'Status',
  '2': const [
    const {'1': 'link', '3': 1, '4': 1, '5': 11, '6': '.proto.link_status.Status.Link', '10': 'link'},
    const {'1': 'response_status_code', '3': 2, '4': 1, '5': 3, '10': 'responseStatusCode'},
    const {'1': 'code_ok', '3': 3, '4': 1, '5': 8, '10': 'codeOk'},
    const {'1': 'code_timeout', '3': 4, '4': 1, '5': 8, '10': 'codeTimeout'},
    const {'1': 'screenshot', '3': 5, '4': 1, '5': 12, '10': 'screenshot'},
    const {'1': 'screenshot_ok', '3': 6, '4': 1, '5': 8, '10': 'screenshotOk'},
    const {'1': 'screenshot_timeout', '3': 7, '4': 1, '5': 8, '10': 'screenshotTimeout'},
  ],
  '3': const [Status_Link$json],
};

const Status_Link$json = const {
  '1': 'Link',
  '2': const [
    const {'1': 'ancestor', '3': 1, '4': 1, '5': 9, '10': 'ancestor'},
    const {'1': 'url', '3': 2, '4': 1, '5': 9, '10': 'url'},
    const {'1': 'text', '3': 3, '4': 1, '5': 9, '10': 'text'},
    const {'1': 'se_site', '3': 4, '4': 1, '5': 9, '10': 'seSite'},
    const {'1': 'post_id', '3': 5, '4': 1, '5': 3, '10': 'postId'},
    const {'1': 'post_type', '3': 6, '4': 1, '5': 3, '10': 'postType'},
  ],
};

