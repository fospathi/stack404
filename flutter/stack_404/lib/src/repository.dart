import 'data.dart';

class Repository {
  final LinkStatusProvider linkStatusProvider;

  Repository(this.linkStatusProvider);

  Stream<LinkStatus> getLinkStatus() {
    return linkStatusProvider.readLinkStatus();
  }
}
