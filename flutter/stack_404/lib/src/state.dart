import 'event.dart';
import 'widget/category_counter/bloc.dart';
import 'widget/link_status_counter/bloc.dart';
import 'widget/link_status_list/bloc.dart';

/// The shared app state.
class SE404AppState {
  SE404AppState();

  final categoryCounter = CategoryCounterState();
  final linkStatusCounter = LinkStatusCounterBloc();
  final linkStatusList = LinkStatusListState();

  addGotLinkStatusEvent(final LinkStatusReceived event) {
    if (!event.linkStatus.isADummyStop) {
      categoryCounter.add(event);
      linkStatusCounter.add(event);
    }
    linkStatusList.add(event);
  }

  close() {
    categoryCounter.close();
    linkStatusCounter.close();
    linkStatusList.close();
  }
}
