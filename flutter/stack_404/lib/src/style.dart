import 'package:flutter/material.dart';

// Main color palette.

// Prussian blue.
const Color prussian = const Color.fromARGB(255, 0, 49, 83);
const Color weakPrussian = const Color.fromARGB(127, 0, 98, 166);
// Cosmic latte.
const Color cosmic = const Color.fromARGB(255, 255, 248, 231);
// School bus yellow.
const Color school = const Color.fromARGB(255, 255, 216, 0);
