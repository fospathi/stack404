import 'package:flutter_bloc/flutter_bloc.dart';

import '../../event.dart';

mixin CategoryCounterBloc on Bloc<LinkStatusReceived, int> {
  Category get category;

  int get initialState => 0;

  Stream<int> mapEventToState(LinkStatusReceived event) async* {
    if (event.category == category) {
      yield state + 1;
    }
    yield state;
  }
}

class Code2xxCounterBloc extends Bloc<LinkStatusReceived, int>
    with CategoryCounterBloc {
  final Category category = Category.code2xx;
}

class Code3xxCounterBloc extends Bloc<LinkStatusReceived, int>
    with CategoryCounterBloc {
  final Category category = Category.code3xx;
}

class Code4xxCounterBloc extends Bloc<LinkStatusReceived, int>
    with CategoryCounterBloc {
  final Category category = Category.code4xx;
}

class Code5xxCounterBloc extends Bloc<LinkStatusReceived, int>
    with CategoryCounterBloc {
  final Category category = Category.code5xx;
}

class CodeXxxCounterBloc extends Bloc<LinkStatusReceived, int>
    with CategoryCounterBloc {
  final Category category = Category.codeXxx;
}

class CategoryCounterState {
  final code2xxCounterBloc = Code2xxCounterBloc();
  final code3xxCounterBloc = Code3xxCounterBloc();
  final code4xxCounterBloc = Code4xxCounterBloc();
  final code5xxCounterBloc = Code5xxCounterBloc();
  final codeXxxCounterBloc = CodeXxxCounterBloc();

  add(LinkStatusReceived event) {
    code2xxCounterBloc.add(event);
    code3xxCounterBloc.add(event);
    code4xxCounterBloc.add(event);
    code5xxCounterBloc.add(event);
    codeXxxCounterBloc.add(event);
  }

  close() {
    code2xxCounterBloc.close();
    code3xxCounterBloc.close();
    code4xxCounterBloc.close();
    code5xxCounterBloc.close();
    codeXxxCounterBloc.close();
  }
}
