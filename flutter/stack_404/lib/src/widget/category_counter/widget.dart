import 'package:badges/badges.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';

import 'bloc.dart';

final Color red = Color.fromARGB(255, 255, 75, 65);

extension CodeCounterBlocBuilder on CategoryCounterState {
  BlocBuilder<Code2xxCounterBloc, int> get code2xxCounterBlocBuilder {
    return BlocBuilder<Code2xxCounterBloc, int>(
        bloc: this.code2xxCounterBloc,
        builder: (context, state) {
          return heading('2XX', Colors.green, state);
        });
  }

  BlocBuilder<Code3xxCounterBloc, int> get code3xxCounterBlocBuilder {
    return BlocBuilder<Code3xxCounterBloc, int>(
        bloc: this.code3xxCounterBloc,
        builder: (context, state) {
          return heading('3XX', Colors.amber, state);
        });
  }

  BlocBuilder<Code4xxCounterBloc, int> get code4xxCounterBlocBuilder {
    return BlocBuilder<Code4xxCounterBloc, int>(
        bloc: this.code4xxCounterBloc,
        builder: (context, state) {
          return heading('4XX', red, state);
        });
  }

  BlocBuilder<Code5xxCounterBloc, int> get code5xxCounterBlocBuilder {
    return BlocBuilder<Code5xxCounterBloc, int>(
        bloc: this.code5xxCounterBloc,
        builder: (context, state) {
          return heading('5XX', red, state);
        });
  }

  BlocBuilder<CodeXxxCounterBloc, int> get codeXxxCounterBlocBuilder {
    return BlocBuilder<CodeXxxCounterBloc, int>(
        bloc: this.codeXxxCounterBloc,
        builder: (context, state) {
          return heading('XXX', Colors.amber, state);
        });
  }
}

const tabHeadingWidth = 75.0;

Widget heading(String heading, Color color, int state) {
  if (0 == state) {
    color = Colors.grey;
  }

  final List<Widget> badges = [];
  if (state < 10) {
    badges.add(Badge(
      badgeContent: Text('$state'),
      badgeColor: color,
      shape: BadgeShape.circle,
    ));
  } else {
    final String digits = '$state';
    for (var i = 0; i < digits.length; i++) {
      final Badge badge = Badge(
        badgeContent: Text(digits[i]),
        badgeColor: color,
        shape: BadgeShape.circle,
      );
      if (i > 0) {
        badges.add(Transform.translate(
          child: badge,
          offset: Offset(-5 * i.toDouble(), 0),
        ));
      } else {
        badges.add(badge);
      }
    }
  }

  return Container(
    width: tabHeadingWidth,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(heading),
        Transform.translate(
          offset: Offset(-3, -14),
          child: Row(children: badges),
        )
      ],
    ),
  );
}
