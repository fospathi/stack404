import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../event.dart';

class LinkStatusCounter extends Equatable {
  LinkStatusCounter(
    this.count,
    this.timeouts,
  );

  LinkStatusCounter.empty()
      : count = 0,
        timeouts = 0;

  factory LinkStatusCounter.fromEvent(
      final LinkStatusReceived event, final LinkStatusCounter other) {
    return LinkStatusCounter(
      other.count + 1,
      event.linkStatus.deadlineExceeded ? other.timeouts + 1 : other.timeouts,
    );
  }

  final int count;
  final int timeouts;

  int get screenshots => count - timeouts;

  List<Object> get props => [count, timeouts];
}

class LinkStatusCounterBloc
    extends Bloc<LinkStatusReceived, LinkStatusCounter> {
  LinkStatusCounter get initialState => LinkStatusCounter.empty();

  Stream<LinkStatusCounter> mapEventToState(final event) async* {
    yield LinkStatusCounter.fromEvent(event, state);
  }
}
