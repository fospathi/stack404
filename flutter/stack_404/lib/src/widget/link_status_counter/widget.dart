import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';

import '../summary_table/widget.dart';
import 'bloc.dart';

extension SummaryTotalsBlocBuilder on LinkStatusCounterBloc {
  BlocBuilder<LinkStatusCounterBloc, LinkStatusCounter>
      get summaryTotalsBlocBuilder {
    return BlocBuilder<LinkStatusCounterBloc, LinkStatusCounter>(
        bloc: this,
        builder: (context, state) {
          return row(state, context);
        });
  }
}

Widget row(final LinkStatusCounter links, final BuildContext context) {
  return Container(
    height: summaryTableRowHeight,
    width: summaryTableWidth,
    child: Row(
      children: [
        Spacer(),
        Expanded(child: topBorderedText('${links.count}', context)),
        Expanded(
          child: topBorderedText(
            '${links.count - links.timeouts} / ${links.count}',
            context,
          ),
        ),
      ],
    ),
  );
}

Widget topBorderedText(String text, final BuildContext context) {
  return Container(
    alignment: Alignment.centerLeft,
    decoration: BoxDecoration(
      border: Border(
        top: BorderSide(color: Theme.of(context).dividerColor, width: 3),
        bottom: BorderSide(color: Theme.of(context).dividerColor, width: 3),
      ),
    ),
    child: Text(text),
  );
}
