import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../event.dart';
import '../../state.dart';
import '../link_status_list/bloc.dart';

class LinkStatusGrid extends StatefulWidget {
  LinkStatusGrid(this.appState, this.category);

  final SE404AppState appState;
  final Category category;

  @override
  _LinkStatusGridState createState() =>
      _LinkStatusGridState(appState, category);
}

class _LinkStatusGridState extends State<LinkStatusGrid> {
  _LinkStatusGridState(this.appState, this.category);

  final _scrollController = ScrollController();
  final SE404AppState appState;
  final Category category;

  Widget build(final BuildContext context) {
    switch (category) {
      case Category.code2xx:
        return gridViewBlocBuilder<Code2xxLinkStatusListBloc>(
          appState.linkStatusList.code2xxLinkStatusListBloc,
          context,
          appState,
          _scrollController,
          "gridViewBlocBuilder<Code2xxLinkStatusListBloc>",
        );

      case Category.code3xx:
        return gridViewBlocBuilder<Code3xxLinkStatusListBloc>(
          appState.linkStatusList.code3xxLinkStatusListBloc,
          context,
          appState,
          _scrollController,
          "gridViewBlocBuilder<Code3xxLinkStatusListBloc>",
        );

      case Category.code4xx:
        return gridViewBlocBuilder<Code4xxLinkStatusListBloc>(
          appState.linkStatusList.code4xxLinkStatusListBloc,
          context,
          appState,
          _scrollController,
          "gridViewBlocBuilder<Code4xxLinkStatusListBloc>",
        );

      case Category.code5xx:
        return gridViewBlocBuilder<Code5xxLinkStatusListBloc>(
          appState.linkStatusList.code5xxLinkStatusListBloc,
          context,
          appState,
          _scrollController,
          "gridViewBlocBuilder<Code5xxLinkStatusListBloc>",
        );

      case Category.codeXxx:
        return gridViewBlocBuilder<CodeXxxLinkStatusListBloc>(
          appState.linkStatusList.codeXxxLinkStatusListBloc,
          context,
          appState,
          _scrollController,
          "gridViewBlocBuilder<CodeXxxLinkStatusListBloc>",
        );

      default:
        return gridViewBlocBuilder<CodeXxxLinkStatusListBloc>(
          appState.linkStatusList.codeXxxLinkStatusListBloc,
          context,
          appState,
          _scrollController,
          "gridViewBlocBuilder<CodeXxxLinkStatusListBloc>",
        );
    }
  }

  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}

BlocBuilder<T, LinkStatusList>
    gridViewBlocBuilder<T extends Bloc<dynamic, LinkStatusList>>(
  final T bloc,
  final BuildContext context,
  final SE404AppState appState,
  final ScrollController scrollController,
  final String key,
) {
  return BlocBuilder<T, LinkStatusList>(
    bloc: bloc,
    builder: (context, state) {
      if (1 == state.links.length && state.links.last.isADummyStop) {
        return Stack();
      } else if (state.links.isEmpty) {
        return Center(
          child: Container(
            width: 40,
            height: 40,
            child: CircularProgressIndicator(),
          ),
        );
      }

      const spacing = 15.0;
      int itemCount;
      if (state.links.last.isADummyStop) {
        itemCount = state.links.length - 1;
      } else {
        itemCount = state.links.length + 1;
      }

      return GridView.builder(
        key: PageStorageKey(key),
        padding: const EdgeInsets.all(spacing),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 1600,
          childAspectRatio: 10 / 7,
          crossAxisSpacing: spacing,
          mainAxisSpacing: spacing,
        ),
        itemBuilder: (BuildContext context, int index) {
          final Duration tooltipWait = Duration(milliseconds: 300);
          if (index >= state.links.length) {
            return Center(
              child: Container(
                width: 40,
                height: 40,
                child: CircularProgressIndicator(),
              ),
            );
          }
          final link = state.links[index];
          Widget code;
          String codeMessage;
          if (!link.codeOk) {
            if (link.codeTimeout) {
              code = Icon(
                Icons.hourglass_empty,
                color: link.codeOk
                    ? Theme.of(context).primaryColor
                    : Theme.of(context).backgroundColor,
                size: 35,
              );
              codeMessage =
                  'Deadline exceeded while trying to determine the HTTP response status code of the link destination';
            } else {
              code = Icon(
                Icons.error_outline,
                color: link.codeOk
                    ? Theme.of(context).primaryColor
                    : Theme.of(context).backgroundColor,
                size: 35,
              );
              codeMessage =
                  'Unable to determine the HTTP response status code of the link destination';
            }
          } else {
            code = Center(
              child: Text(
                '${link.responseStatusCode}',
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.bold,
                ),
                textScaleFactor: 1.4,
              ),
            );
            codeMessage =
                'HTTP response status code of the link destination.\nTap to visit a description of status codes.';
          }
          return Card(
            elevation: 3,
            child: Column(
              children: [
                Expanded(
                  flex: 1,
                  child: Stack(
                    children: [
                      ListTile(
                        contentPadding: EdgeInsets.fromLTRB(10, 5, 70, 0),
                        leading: SizedBox.fromSize(
                          size: Size(50, 50),
                          child: ClipOval(
                            child: Material(
                              color: link.codeOk
                                  ? Theme.of(context).backgroundColor
                                  : Theme.of(context).primaryColor,
                              child: Tooltip(
                                message: codeMessage,
                                waitDuration: tooltipWait,
                                child: !link.codeOk
                                    ? code
                                    : InkWell(
                                        onTap: () => launch(
                                            'https://developer.mozilla.org/en-US/docs/Web/HTTP/Status'),
                                        hoverColor:
                                            Color.fromARGB(127, 255, 248, 231),
                                        child: code,
                                      ),
                              ),
                            ),
                          ),
                        ),
                        title: Wrap(
                          direction: Axis.horizontal,
                          children: [
                            Tooltip(
                              message: 'Visit the link destination',
                              waitDuration: tooltipWait,
                              child: InkWell(
                                child: Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(0, 8, 8, 8),
                                  child: Text(
                                    link.url,
                                    maxLines: 2,
                                    overflow: TextOverflow.clip,
                                    style: TextStyle(
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                ),
                                onTap: () => launch(link.url),
                              ),
                            ),
                          ],
                        ),
                        subtitle: Wrap(
                          direction: Axis.horizontal,
                          children: [
                            Tooltip(
                              message:
                                  'Context for the link source.\nFor an <a> element this is the text content, if any.\nFor a raw link this is the surrounding text, if any.',
                              waitDuration: tooltipWait,
                              child: Container(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
                                child: Text(
                                  link.text,
                                  maxLines: 3,
                                  overflow: TextOverflow.clip,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: Card(
                          margin: EdgeInsets.all(0),
                          elevation: 0,
                          color: Theme.of(context).backgroundColor,
                          child: Wrap(
                            crossAxisAlignment: WrapCrossAlignment.center,
                            direction: Axis.vertical,
                            children: [
                              Tooltip(
                                message: link.isQuestion
                                    ? 'Visit the link source in a question post at ${link.seSite}'
                                    : 'Visit the link source in an answer post at ${link.seSite}',
                                waitDuration: tooltipWait,
                                child: InkWell(
                                  child: IconButton(
                                    iconSize: 40,
                                    color: Theme.of(context).primaryColor,
                                    hoverColor:
                                        Color.fromARGB(127, 255, 248, 231),
                                    icon: Icon(Icons.chat),
                                    onPressed: () => launch(link.postUrl),
                                  ),
                                ),
                              ),
                              Tooltip(
                                message: 'Visit the Wayback Machine',
                                waitDuration: tooltipWait,
                                child: InkWell(
                                  child: IconButton(
                                    iconSize: 35,
                                    color: Theme.of(context).primaryColor,
                                    hoverColor:
                                        Color.fromARGB(127, 255, 248, 231),
                                    icon: Icon(Icons.local_library),
                                    onPressed: () => launch(link.archiveUrl),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: !link.screenshotOk
                      ? Container(
                          decoration: BoxDecoration(
                            border: Border(
                              top: BorderSide(
                                color: Theme.of(context).dividerColor,
                              ),
                            ),
                          ),
                          child: Center(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(link.screenshotTimeout
                                    ? 'Sorry, the attempt to get a screenshot of the link destination exceeded the deadline.'
                                    : 'Sorry, the attempt to get a screenshot of the link destination did not succeed.'),
                                link.screenshotTimeout
                                    ? Icon(Icons.hourglass_empty)
                                    : Icon(Icons.broken_image),
                              ],
                            ),
                          ),
                        )
                      : Container(
                          margin: EdgeInsets.all(0),
                          padding: EdgeInsets.symmetric(vertical: 15),
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            border: Border(
                              top: BorderSide(
                                color: Theme.of(context).dividerColor,
                              ),
                            ),
                          ),
                          child: Align(
                            alignment: Alignment.center,
                            child: Tooltip(
                              message: 'Screenshot of the link destination',
                              waitDuration: tooltipWait,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Theme.of(context).dividerColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(21.0),
                                  shape: BoxShape.rectangle,
                                ),
                                child: ClipRRect(
                                  child: link.screenshot,
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                ),
              ],
            ),
          );
        },
        itemCount: itemCount,
        controller: scrollController,
      );
    },
  );
}
