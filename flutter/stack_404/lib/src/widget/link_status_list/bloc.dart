import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data.dart';
import '../../event.dart';

class LinkStatusList extends Equatable {
  LinkStatusList(
    this.count,
    this.timeouts,
    this.links,
  );

  LinkStatusList.empty()
      : count = 0,
        timeouts = 0,
        links = [];

  factory LinkStatusList.fromEvent(
      final LinkStatusReceived event, final LinkStatusList other) {
    if (!event.linkStatus.isADummyStop) {
      return LinkStatusList(
        other.count + 1,
        event.linkStatus.deadlineExceeded ? other.timeouts + 1 : other.timeouts,
        List.from(other.links)..add(event.linkStatus),
      );
    }
    return LinkStatusList(
      other.count,
      other.timeouts,
      List.from(other.links)..add(event.linkStatus),
    );
  }

  final int count;
  final int timeouts;
  final List<LinkStatus> links;

  List<Object> get props => [count, timeouts, links];
}

mixin CategoryLinks on Bloc<LinkStatusReceived, LinkStatusList> {
  Category get category;

  LinkStatusList get initialState => LinkStatusList.empty();

  Stream<LinkStatusList> mapEventToState(final event) async* {
    if (event.category == category) {
      yield LinkStatusList.fromEvent(event, state);
    }
    yield state;
  }
}

class Code2xxLinkStatusListBloc extends Bloc<LinkStatusReceived, LinkStatusList>
    with CategoryLinks {
  final Category category = Category.code2xx;
}

class Code3xxLinkStatusListBloc extends Bloc<LinkStatusReceived, LinkStatusList>
    with CategoryLinks {
  final Category category = Category.code3xx;
}

class Code4xxLinkStatusListBloc extends Bloc<LinkStatusReceived, LinkStatusList>
    with CategoryLinks {
  final Category category = Category.code4xx;
}

class Code5xxLinkStatusListBloc extends Bloc<LinkStatusReceived, LinkStatusList>
    with CategoryLinks {
  final Category category = Category.code5xx;
}

class CodeXxxLinkStatusListBloc extends Bloc<LinkStatusReceived, LinkStatusList>
    with CategoryLinks {
  final Category category = Category.codeXxx;
}

class LinkStatusListState {
  final code2xxLinkStatusListBloc = Code2xxLinkStatusListBloc();
  final code3xxLinkStatusListBloc = Code3xxLinkStatusListBloc();
  final code4xxLinkStatusListBloc = Code4xxLinkStatusListBloc();
  final code5xxLinkStatusListBloc = Code5xxLinkStatusListBloc();
  final codeXxxLinkStatusListBloc = CodeXxxLinkStatusListBloc();

  add(LinkStatusReceived event) {
    if (event.linkStatus.isADummyStop) {
      code2xxLinkStatusListBloc
          .add(LinkStatusReceived(LinkStatus.dummyStop(250)));
      code3xxLinkStatusListBloc
          .add(LinkStatusReceived(LinkStatus.dummyStop(350)));
      code4xxLinkStatusListBloc
          .add(LinkStatusReceived(LinkStatus.dummyStop(450)));
      code5xxLinkStatusListBloc
          .add(LinkStatusReceived(LinkStatus.dummyStop(550)));
      codeXxxLinkStatusListBloc
          .add(LinkStatusReceived(LinkStatus.dummyStop(50)));
    } else {
      code2xxLinkStatusListBloc.add(event);
      code3xxLinkStatusListBloc.add(event);
      code4xxLinkStatusListBloc.add(event);
      code5xxLinkStatusListBloc.add(event);
      codeXxxLinkStatusListBloc.add(event);
    }
  }

  close() {
    code2xxLinkStatusListBloc.close();
    code3xxLinkStatusListBloc.close();
    code4xxLinkStatusListBloc.close();
    code5xxLinkStatusListBloc.close();
    codeXxxLinkStatusListBloc.close();
  }
}
