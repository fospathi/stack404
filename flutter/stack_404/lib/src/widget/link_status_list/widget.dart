import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';

import '../summary_table/widget.dart';
import 'bloc.dart';

bool condition(final LinkStatusList prev, final LinkStatusList current) {
  if (current.links.last.isADummyStop) {
    return false;
  }
  return true;
}

extension SummaryRowBlocBuilder on LinkStatusListState {
  BlocBuilder<Code2xxLinkStatusListBloc, LinkStatusList>
      get code2xxSummaryRowBlocBuilder {
    return BlocBuilder<Code2xxLinkStatusListBloc, LinkStatusList>(
        bloc: this.code2xxLinkStatusListBloc,
        condition: condition,
        builder: (context, state) {
          return row('2XX', state);
        });
  }

  BlocBuilder<Code3xxLinkStatusListBloc, LinkStatusList>
      get code3xxSummaryRowBlocBuilder {
    return BlocBuilder<Code3xxLinkStatusListBloc, LinkStatusList>(
        bloc: this.code3xxLinkStatusListBloc,
        condition: condition,
        builder: (context, state) {
          return row('3XX', state);
        });
  }

  BlocBuilder<Code4xxLinkStatusListBloc, LinkStatusList>
      get code4xxSummaryRowBlocBuilder {
    return BlocBuilder<Code4xxLinkStatusListBloc, LinkStatusList>(
        bloc: this.code4xxLinkStatusListBloc,
        condition: condition,
        builder: (context, state) {
          return row('4XX', state);
        });
  }

  BlocBuilder<Code5xxLinkStatusListBloc, LinkStatusList>
      get code5xxSummaryRowBlocBuilder {
    return BlocBuilder<Code5xxLinkStatusListBloc, LinkStatusList>(
        bloc: this.code5xxLinkStatusListBloc,
        condition: condition,
        builder: (context, state) {
          return row('5XX', state);
        });
  }

  BlocBuilder<CodeXxxLinkStatusListBloc, LinkStatusList>
      get codeXxxSummaryRowBlocBuilder {
    return BlocBuilder<CodeXxxLinkStatusListBloc, LinkStatusList>(
        bloc: this.codeXxxLinkStatusListBloc,
        condition: condition,
        builder: (context, state) {
          return row('XXX', state);
        });
  }
}

Widget row(String heading, LinkStatusList links) {
  return Container(
    height: summaryTableRowHeight,
    width: summaryTableWidth,
    child: Row(
      children: [
        Expanded(child: Text(heading)),
        Expanded(child: Text('${links.count}')),
        Expanded(
            child: Text('${links.count - links.timeouts} / ${links.count}')),
      ],
    ),
  );
}
