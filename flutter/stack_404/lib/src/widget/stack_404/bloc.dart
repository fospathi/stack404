import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data.dart';
import '../../event.dart';
import '../../repository.dart';
import '../../state.dart';

class Stack404Bloc extends Bloc<ConditionChanged, Condition> {
  Stack404Bloc();

  final appState = SE404AppState();
  final repository = Repository(LinkStatusProvider());

  Function onLoadingLinkStatus;

  Condition get initialState => Condition.se404Initial;

  Stream<Condition> mapEventToState(ConditionChanged event) async* {
    switch (state) {
      case Condition.se404Initial:
        if (event is Stack404Started) {
          repository.getLinkStatus().listen((final LinkStatus status) {
            appState.addGotLinkStatusEvent(LinkStatusReceived(status));
          });
          add(SE404LinkStatusLoading());
          yield Condition.linkStatusLoadInProgress;
        }
        break;

      case Condition.linkStatusLoadInProgress:
        await new Future.delayed(const Duration(seconds: 5));
        if (event is SE404LinkStatusLoading) {
          onLoadingLinkStatus?.call();
        }
        break;
    }
    yield state;
  }
}

// The top level app condition.
enum Condition {
  se404Initial,
  linkStatusLoadInProgress,
}
