import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';

import '../../event.dart';
import '../../state.dart';
import '../../style.dart';
import '../category_counter/widget.dart';
import '../link_status_grid/widget.dart';
import '../summary_table/widget.dart';
import 'bloc.dart';

extension SE404BlocBuilder on Stack404Bloc {
  BlocBuilder<Stack404Bloc, Condition> get se404BlocBuilder {
    return BlocBuilder<Stack404Bloc, Condition>(
        bloc: this,
        builder: (context, state) {
          switch (state) {
            case Condition.se404Initial:
              print("drawing for SE404Initial");
              return SimpleWidget('initialising...');

            case Condition.linkStatusLoadInProgress:
              print("drawing for LinkStatusLoadInProgress");
              return SE404Widget(appState);
          }

          return SimpleWidget('unknown condition error');
        });
  }
}

class SimpleWidget extends StatelessWidget {
  SimpleWidget(this.message);

  final String message;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stack404',
      home: Scaffold(
        body: Center(
          child: Text(message),
        ),
      ),
    );
  }
}

class SE404Widget extends StatelessWidget {
  SE404Widget(this.appState);

  final SE404AppState appState;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stack404',
      theme: ThemeData(
        primaryColor: cosmic,
        accentColor: school,
        dividerColor: weakPrussian,
        backgroundColor: prussian,
      ),
      home: DefaultTabController(
        length: 6,
        child: Scaffold(
          backgroundColor: cosmic,
          appBar: AppBar(
            title: const Text('Stack404', style: TextStyle(color: prussian)),
            backgroundColor: cosmic,
            centerTitle: true,
            bottom: TabBar(
              labelColor: prussian,
              indicatorWeight: 5,
              labelStyle: TextStyle(color: prussian),
              isScrollable: true,
              tabs: [
                Container(
                  width: tabHeadingWidth,
                  child: Tab(icon: Icon(Icons.home), text: "Summary"),
                ),
                Tab(child: appState.categoryCounter.code2xxCounterBlocBuilder),
                Tab(child: appState.categoryCounter.code3xxCounterBlocBuilder),
                Tab(child: appState.categoryCounter.code4xxCounterBlocBuilder),
                Tab(child: appState.categoryCounter.code5xxCounterBlocBuilder),
                Tab(child: appState.categoryCounter.codeXxxCounterBlocBuilder),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              SummaryTableWidget(appState),
              LinkStatusGrid(appState, Category.code2xx),
              LinkStatusGrid(appState, Category.code3xx),
              LinkStatusGrid(appState, Category.code4xx),
              LinkStatusGrid(appState, Category.code5xx),
              LinkStatusGrid(appState, Category.codeXxx),
            ],
          ),
        ),
      ),
    );
  }
}
