import 'package:flutter/material.dart';

import '../../state.dart';
import '../link_status_counter/widget.dart';
import '../link_status_list/widget.dart';

const double summaryTableRowHeight = 40;
const double summaryTableWidth = 350;

class SummaryTableWidget extends StatelessWidget {
  SummaryTableWidget(this.appState);

  final SE404AppState appState;

  Widget build(BuildContext context) {
    Color dividerColor = Theme.of(context).dividerColor;
    return Center(
      child: Container(
        padding: EdgeInsets.all(20),
        width: summaryTableWidth,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: dividerColor,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(20.0),
          shape: BoxShape.rectangle,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              height: summaryTableRowHeight,
              width: summaryTableWidth,
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: dividerColor, width: 3),
                  top: BorderSide(color: dividerColor, width: 3),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(child: Text(("Status class"))),
                  Expanded(child: Text("Total")),
                  Expanded(
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: Icon(Icons.photo_camera)),
                  ),
                ],
              ),
            ),
            appState.linkStatusList.code2xxSummaryRowBlocBuilder,
            appState.linkStatusList.code3xxSummaryRowBlocBuilder,
            appState.linkStatusList.code4xxSummaryRowBlocBuilder,
            appState.linkStatusList.code5xxSummaryRowBlocBuilder,
            appState.linkStatusList.codeXxxSummaryRowBlocBuilder,
            appState.linkStatusCounter.summaryTotalsBlocBuilder,
          ],
        ),
      ),
    );
  }
}
