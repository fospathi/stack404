module gitlab.com/fospathi/stack404

go 1.14

require (
	github.com/asaskevich/govalidator v0.0.0-20200428143746-21a406dcc535
	github.com/golang/protobuf v1.4.1
	gitlab.com/fospathi/cet v0.0.0-20200309200421-354e9aef3679
	gitlab.com/fospathi/watchable v0.0.0-20200506205313-4a70bd2b312f
	golang.org/x/net v0.0.0-20200506145744-7e3656a0809f
)
