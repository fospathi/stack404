/*
Package links provides link related utilities.
*/
package links

import (
	"regexp"

	"github.com/asaskevich/govalidator"
	"gitlab.com/fospathi/cet"
	"gitlab.com/fospathi/stack404/config"
	"gitlab.com/fospathi/stack404/link"
	"gitlab.com/fospathi/stack404/stack404sites"
	"golang.org/x/net/html"
)

// RemoveDuplicates from the argument slice, keeping the first occurrence.
func RemoveDuplicates(vl []link.L) []link.L {
	c := cet.NewS()
	for i := 0; i < len(vl); {
		l := vl[i]
		if c.Contains(l.URL()) {
			vl = vl[:i+copy(vl[i:], vl[i+1:])]
		} else {
			c.Add(l.URL())
			i++
		}
	}
	return vl
}

// KeepExternal filters out both the internal and invalid links from the argument slice returning a slice of
// any remaining external valid links using the same underlying array as the argument slice.
func KeepExternal(vl []link.L) []link.L {
	for i := 0; i < len(vl); i++ {
		l := vl[i]
		ext, err := stack404sites.IsExternal(l.URL())
		if err != nil || !ext { // Remove invalid and internal links
			vl = append(vl[:i], vl[i+1:]...)
		}
	}
	return vl
}

// Extract the links (such as URLs in code comments in a code element) and hyperlinks (anchors) in the
// argument node's tree, that is, the tree with it as the root node.
func Extract(doc *html.Node) []link.L {
	// Note: Use raw strings to avoid having to quote the backslashes.

	// This will, as part of a regexp string, match one or more traditionally-valid URL characters.
	const Chs string = `[A-Za-z0-9-_~:/#[@!$&',;=\.\*\?\+\(\)\]]+`

	// A rough URL-detecting regexp string. Not all matches will be valid URLs. Some valid URLs will not be
	// matched.
	//
	// To play with this regexp see: <https://play.golang.org/p/HQLiwRWNfKQ>.
	const URLRegexpString = `(` + `(https?://)` + Chs + `)|` +
		`(` + `\bwww\.` + Chs + `)|` +
		`(` + Chs + `.com\b` + `)|` +
		`(` + Chs + `.com/` + Chs + `)`

	roughURL := regexp.MustCompile(URLRegexpString)

	// Find the first ancestor element, if any, that tends to dominate the presentation of its contents. These
	// elements are li, code, and blockquote.
	var getAncestor func(*html.Node) string
	getAncestor = func(n *html.Node) string {
		if html.ElementNode == n.Type {
			switch n.Data {
			case "blockquote", "code", "li":
				return n.Data
			}
		}
		if nil == n.Parent {
			return ""
		}
		return getAncestor(n.Parent)
	}

	isAnAnchor := func(n *html.Node) (ok bool, href string, text string) {
		if "a" == n.Data {
			for _, attr := range n.Attr {
				if "href" == attr.Key {
					ok, href = true, attr.Val
					if c := n.FirstChild; c != nil {
						text = c.Data
					}
					return
				}
			}
		}
		return
	}

	var scan func([]link.L, *html.Node) []link.L
	scan = func(vl []link.L, n *html.Node) []link.L {
		switch n.Type {
		case html.ElementNode:
			if ok, href, text := isAnAnchor(n); ok {
				vl = append(vl, hyperlink{ancestor: getAncestor(n), href: href, text: text})
			}
		case html.TextNode:
			if matches := roughURL.FindAllStringIndex(n.Data, -1); len(matches) > 0 {
				l := len(n.Data)
				for i := 0; i < len(matches); i++ {
					j1, j2 := matches[i][0], matches[i][1]
					if url := n.Data[j1:j2]; govalidator.IsURL(url) {
						j0, j3 := maxInt(j1-config.Pad, 0), minInt(j2+config.Pad, l)
						vl = append(vl, rawlink{
							ancestor: getAncestor(n),
							context:  n.Data[j0:j3],
							url:      url,
							c1:       j1 - j0,
							c2:       j2 - j0})
					}
				}
			}
		default:
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			vl = scan(vl, c)
		}
		return vl
	}

	return scan([]link.L{}, doc)
}

// An anchor.
type hyperlink struct {
	ancestor string
	href     string
	text     string // An anchor's text may be empty.
}

func (l hyperlink) Ancestor() string {
	return l.ancestor
}

func (l hyperlink) URL() string {
	return l.href
}

func (l hyperlink) String() string {
	if len(l.text) > 0 {
		return l.text
	}
	return l.href
}

// A link which does not correspond to a HTML node/anchor; it's just some text that matches a URL-detecting
// regexp.
type rawlink struct {
	ancestor string
	context  string
	c1, c2   int // The slice indices of the URL within the context string.
	url      string
}

func (l rawlink) Ancestor() string {
	return l.ancestor
}

func (l rawlink) URL() string {
	return l.url
}

func (l rawlink) String() string {
	return l.context
}

func maxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func minInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}
