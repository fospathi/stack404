package links_test

import (
	"log"
	"strings"
	"testing"

	"golang.org/x/net/html"

	"gitlab.com/fospathi/stack404/links"
)

func Test_KeepExternal(t *testing.T) {
	fragment := `<p>Link 1: <a href="google.com"></a>, Link 2: <a href="stackexchange.com"></a></p>`
	doc, err := html.Parse(strings.NewReader("<div>" + fragment + "</div>"))
	if err != nil {
		log.Fatal(err)
	}
	vh := links.KeepExternal(links.Extract(doc))

	want1, want2 := 1, "google.com"
	got1, got2 := len(vh), vh[0].URL()
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}
}
