package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/fospathi/stack404/config"
	"gitlab.com/fospathi/stack404/firecrow"
	"gitlab.com/fospathi/stack404/link"
	"gitlab.com/fospathi/stack404/links"
	"gitlab.com/fospathi/stack404/record"
	"gitlab.com/fospathi/stack404/status"
	"gitlab.com/fospathi/watchable"
)

func main() {
	fatal(firecrow.ResetFire())
	defer firecrow.ResetFire()

	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(http.Dir(config.FilesPath)))
	ps := watchable.NewPubsub()
	//cancel, err := ps.ListenAndServeAnyPortOnWebsocket(config.SrvAddr, config.SrvPath)
	cancel, err := ps.ListenAndServeOnWebsocket(mux, config.SrvAddr, config.WSPath)
	fatal(err)
	defer cancel()

	f, err := os.Open(config.QueryResultsPath)
	fatal(err)
	r := csv.NewReader(f)
	_, err = r.Read() // Skip over the CSV file's column titles.
	fatal(err)

	fmt.Printf("stack404: visit " + link.SchemifyAs(config.SrvAddr, false) + "\n")
	n := 0
	for {
		site, postID, postType, doc, ok, err := record.Parse(r)
		fatal(err)
		if !ok {
			break
		}

		externalLinks := links.RemoveDuplicates(links.KeepExternal(links.Extract(doc)))
		n += len(externalLinks)
		publisher := func(st link.Status) {
			st.SeSite = site
			st.PostID = postID
			st.PostType = postType
			ps.Pub(st)
		}
		fatal(status.OfSites(publisher, externalLinks...))
	}
	ps.Complete()

	fmt.Printf("stack404: scrutinisation of %v unique prospective external links is complete.\n", n)
	<-make(chan struct{})
}

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
