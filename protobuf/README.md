# Protocol buffers

Here you will find the definitions of data structures used by the app for data interchange between the app's backend and frontend components. The data structures are defined in terms of the Protocol Buffers data interchange format.

See the doc comments inside the *.proto* files for information about the data structures.

## Dependencies

To be able to regenerate code after a change to a *.proto* file the following dependencies will need to be met:

* [Protocol Buffers](https://developers.google.com/protocol-buffers).
* For Dart files: [Dart plugin for the protoc compiler](https://pub.dev/packages/protoc_plugin).
* For Go files: [Go support for Protocol Buffers](https://github.com/golang/protobuf/).

## Regenerating the source code files

To synchronise code with data structure definitions: from a terminal, with the current working directory as this directory, execute the following commands:

* **link_status.proto**:
  ```sh
  protoc --proto_path=. --dart_out=../flutter/stack_404/lib/src/protobuf/link_status link_status.proto
  protoc --go_out=../link/protolink link_status.proto
  ```
