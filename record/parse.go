package record

import (
	"encoding/csv"
	"errors"
	"io"
	"strconv"
	"strings"

	"gitlab.com/fospathi/stack404/stack404sites"
	"golang.org/x/net/html"
)

// Parse the next single record from a CSV file which was obtained by using the special TSQL query on the
// StackExchange Data Explorer.
//
// ok is true when a record is parsed and false when no records remain.
//
// Whereas the argument record's HTML-as-a-string type variable is the inner HTML of some element, the
// returned doc is a proper HTML document: it is enclosed in a div element before being passed to the HTML
// parser and the HTML parser adds elements as well.
func Parse(r *csv.Reader) (site string, postID int, postType int, doc *html.Node, ok bool, err error) {
	record, err := r.Read()
	if err != nil {
		if err == io.EOF {
			err = nil
		}
		return
	}

	parseDBName := func(i int) error {
		site, ok = stack404sites.URL(record[i])
		if !ok {
			return errors.New("invalid database name")
		}
		return nil
	}

	parsePostID := func(i int) error {
		postID, err = strconv.Atoi(record[i])
		return err
	}

	parsePostType := func(i int) error {
		postType, err = strconv.Atoi(record[i])
		return err
	}

	parseDoc := func(i int) error {
		doc, err = html.Parse(strings.NewReader("<div>" + record[i] + "</div>"))
		return err
	}

	for i, f := range []func(int) error{parseDBName, parsePostID, parsePostType, parseDoc} {
		err = f(i)
		if err != nil {
			return
		}
	}

	ok = true
	return
}
