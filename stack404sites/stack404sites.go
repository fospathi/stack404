/*
Package stack404sites knows the URLs of the Stack Exchange Q&A sites found at
https://stackexchange.com/sites?view=list#name.
*/
package stack404sites

import (
	"fmt"
	"net/url"
	"regexp"

	"gitlab.com/fospathi/stack404/link"
)

// Contains checks whether the hostname of the argument valid URL is exactly equal to one of the Stack Exchange
// Q&A sites' hostnames.
func Contains(u string) (bool, error) {
	// Prep u for url.Parse: needs a scheme.
	u = link.Schemify(u)
	// Strip (www)s.
	if regexp.MustCompile(`^https?://www\.`).MatchString(u) {
		if m := regexp.MustCompile(`^(https?://)www\.(.+)`).FindStringSubmatch(u); 3 == len(m) {
			u = m[1] + m[2]
		}
	}
	if _, err := getAddress(u); err != nil {
		return false, err
	}
	parsedURL, _ := url.Parse(u)
	hostname := parsedURL.Hostname()
	for _, site := range all {
		if parsedSite, err := url.Parse(site); hostname == parsedSite.Hostname() {
			if err != nil {
				return false, err
			}
			return true, nil
		}
	}
	return false, nil
}

// IsExternal returns whether the argument valid URL is an external link (where an external link is neither a
// general Stack Exchange site nor a Stack Exchange Q&A site).
func IsExternal(u string) (bool, error) {
	ua, err := getAddress(u)
	if err != nil {
		return false, err
	}
	if addresses[ua] {
		return false, nil
	}
	return true, nil
}

// URL of a Stack Exchange Q&A site whose name is given by the exact name argument.
//
// The argument name, if it is a site name, shall be an exact everything-sensitive copy of the name as it
// appears on the list of Stack Exchange Q&A sites at https://stackexchange.com/sites?view=list#name>.
//
// The argument name, if it is a database name, shall be an exact everything-sensitive copy of the name as it
// output by the Stack Exchange Data Explorer at
// https://data.stackexchange.com/stackoverflow/query/1164703/mapping-for-database-qa-site-url.
func URL(name string) (string, bool) {
	if url, ok := all[name]; ok {
		return url, true
	}
	if url, ok := dburls[name]; ok {
		return url, true
	}
	return "", false
}

var addressRegexp = regexp.MustCompile(`.*?([[:alpha:]]+\.[[:alpha:]]+)$`)

// Get the address (domain + top level domain) of the argument URL.
//
// An address here means the domain and top level domain, i.e. "stackoverflow.com", not
// "es.stackoverflow.com".
func getAddress(u string) (string, error) {
	u = link.Schemify(u)
	parsedURL, err := url.Parse(u)
	if err != nil {
		return "", err
	}
	m := addressRegexp.FindStringSubmatch(parsedURL.Hostname())
	if len(m) < 2 {
		return "", fmt.Errorf("malformed hostname in URL: %v", u)
	}
	return m[1], nil
}

// Stores the Stack Exchange Q&A sites' addresses as keys without the trailing slash `/`. An address here means
// the main domain and the top level domain, i.e. "stackoverflow.com", not "es.stackoverflow.com".
var addresses = func() map[string]bool {
	ma := map[string]bool{}
	for _, u := range all {
		a, _ := getAddress(u)
		ma[a] = true
	}
	return ma
}()

// All the Stack Exchange Q&A sites. Keys are names and values are URLs.
//
// Excludes the meta sub sites except for Meta Stack Exchange.
var all = map[string]string{
	`3D Printing`:                          `https://3dprinting.stackexchange.com/`,
	`Academia`:                             `https://academia.stackexchange.com/`,
	`Amateur Radio`:                        `https://ham.stackexchange.com/`,
	`Android Enthusiasts`:                  `https://android.stackexchange.com/`,
	`Anime & Manga`:                        `https://anime.stackexchange.com/`,
	`Arduino`:                              `https://arduino.stackexchange.com/`,
	`Arqade`:                               `https://gaming.stackexchange.com/`,
	`Artificial Intelligence`:              `https://ai.stackexchange.com/`,
	`Arts & Crafts`:                        `https://crafts.stackexchange.com/`,
	`Ask Different`:                        `https://apple.stackexchange.com/`,
	`Ask Patents`:                          `https://patents.stackexchange.com/`,
	`Ask Ubuntu`:                           `https://askubuntu.com/`,
	`Astronomy`:                            `https://astronomy.stackexchange.com/`,
	`Aviation`:                             `https://aviation.stackexchange.com/`,
	`Beer, Wine & Spirits`:                 `https://alcohol.stackexchange.com/`,
	`Biblical Hermeneutics`:                `https://hermeneutics.stackexchange.com/`,
	`Bicycles`:                             `https://bicycles.stackexchange.com/`,
	`Bioinformatics`:                       `https://bioinformatics.stackexchange.com/`,
	`Biology`:                              `https://biology.stackexchange.com/`,
	`Bitcoin`:                              `https://bitcoin.stackexchange.com/`,
	`Blender`:                              `https://blender.stackexchange.com/`,
	`Board & Card Games`:                   `https://boardgames.stackexchange.com/`,
	`Bricks`:                               `https://bricks.stackexchange.com/`,
	`Buddhism`:                             `https://buddhism.stackexchange.com/`,
	`Chemistry`:                            `https://chemistry.stackexchange.com/`,
	`Chess`:                                `https://chess.stackexchange.com/`,
	`Chinese Language`:                     `https://chinese.stackexchange.com/`,
	`Christianity`:                         `https://christianity.stackexchange.com/`,
	`CiviCRM`:                              `https://civicrm.stackexchange.com/`,
	`Code Golf`:                            `https://codegolf.stackexchange.com/`,
	`Code Review`:                          `https://codereview.stackexchange.com/`,
	`Coffee`:                               `https://coffee.stackexchange.com/`,
	`Community Building`:                   `https://communitybuilding.stackexchange.com/`,
	`Computational Science`:                `https://scicomp.stackexchange.com/`,
	`Computer Graphics`:                    `https://computergraphics.stackexchange.com/`,
	`Computer Science Educators`:           `https://cseducators.stackexchange.com/`,
	`Computer Science`:                     `https://cs.stackexchange.com/`,
	`Constructed Languages`:                `https://conlang.stackexchange.com/`,
	`Craft CMS`:                            `https://craftcms.stackexchange.com/`,
	`Cross Validated`:                      `https://stats.stackexchange.com/`,
	`Cryptography`:                         `https://crypto.stackexchange.com/`,
	`Data Science`:                         `https://datascience.stackexchange.com/`,
	`Database Administrators`:              `https://dba.stackexchange.com/`,
	`DevOps`:                               `https://devops.stackexchange.com/`,
	`Drones and Model Aircraft`:            `https://drones.stackexchange.com/`,
	`Drupal Answers`:                       `https://drupal.stackexchange.com/`,
	`EOS.IO`:                               `https://eosio.stackexchange.com/`,
	`Earth Science`:                        `https://earthscience.stackexchange.com/`,
	`Ebooks`:                               `https://ebooks.stackexchange.com/`,
	`Economics`:                            `https://economics.stackexchange.com/`,
	`Electrical Engineering`:               `https://electronics.stackexchange.com/`,
	`Emacs`:                                `https://emacs.stackexchange.com/`,
	`Engineering`:                          `https://engineering.stackexchange.com/`,
	`English Language & Usage`:             `https://english.stackexchange.com/`,
	`English Language Learners`:            `https://ell.stackexchange.com/`,
	`Esperanto Language`:                   `https://esperanto.stackexchange.com/`,
	`Ethereum`:                             `https://ethereum.stackexchange.com/`,
	`Expatriates`:                          `https://expatriates.stackexchange.com/`,
	`ExpressionEngine® Answers`:            `https://expressionengine.stackexchange.com/`,
	`Freelancing`:                          `https://freelancing.stackexchange.com/`,
	`French Language`:                      `https://french.stackexchange.com/`,
	`Game Development`:                     `https://gamedev.stackexchange.com/`,
	`Gardening & Landscaping`:              `https://gardening.stackexchange.com/`,
	`Genealogy & Family History`:           `https://genealogy.stackexchange.com/`,
	`Geographic Information Systems`:       `https://gis.stackexchange.com/`,
	`German Language`:                      `https://german.stackexchange.com/`,
	`Graphic Design`:                       `https://graphicdesign.stackexchange.com/`,
	`Hardware Recommendations`:             `https://hardwarerecs.stackexchange.com/`,
	`Hinduism`:                             `https://hinduism.stackexchange.com/`,
	`History`:                              `https://history.stackexchange.com/`,
	`History of Science and Mathematics`:   `https://hsm.stackexchange.com/`,
	`Home Improvement`:                     `https://diy.stackexchange.com/`,
	`Homebrewing`:                          `https://homebrew.stackexchange.com/`,
	`Information Security`:                 `https://security.stackexchange.com/`,
	`Internet of Things`:                   `https://iot.stackexchange.com/`,
	`Interpersonal Skills`:                 `https://interpersonal.stackexchange.com/`,
	`Iota`:                                 `https://iota.stackexchange.com/`,
	`Islam`:                                `https://islam.stackexchange.com/`,
	`Italian Language`:                     `https://italian.stackexchange.com/`,
	`Japanese Language`:                    `https://japanese.stackexchange.com/`,
	`Joomla`:                               `https://joomla.stackexchange.com/`,
	`Korean Language`:                      `https://korean.stackexchange.com/`,
	`Language Learning`:                    `https://languagelearning.stackexchange.com/`,
	`Latin Language`:                       `https://latin.stackexchange.com/`,
	`Law`:                                  `https://law.stackexchange.com/`,
	`Lifehacks`:                            `https://lifehacks.stackexchange.com/`,
	`Linguistics`:                          `https://linguistics.stackexchange.com/`,
	`Literature`:                           `https://literature.stackexchange.com/`,
	`Magento`:                              `https://magento.stackexchange.com/`,
	`Martial Arts`:                         `https://martialarts.stackexchange.com/`,
	`Materials Modeling`:                   `https://materials.stackexchange.com/`,
	`MathOverflow`:                         `https://mathoverflow.net/`,
	`Mathematica`:                          `https://mathematica.stackexchange.com/`,
	`Mathematics Educators`:                `https://matheducators.stackexchange.com/`,
	`Mathematics`:                          `https://math.stackexchange.com/`,
	`Medical Sciences`:                     `https://medicalsciences.stackexchange.com/`,
	`Meta Stack Exchange`:                  `https://meta.stackexchange.com/`,
	`Mi Yodeya`:                            `https://judaism.stackexchange.com/`,
	`Monero`:                               `https://monero.stackexchange.com/`,
	`Motor Vehicle Maintenance & Repair`:   `https://mechanics.stackexchange.com/`,
	`Movies & TV`:                          `https://movies.stackexchange.com/`,
	`Music Fans`:                           `https://musicfans.stackexchange.com/`,
	`Music: Practice & Theory`:             `https://music.stackexchange.com/`,
	`Mythology & Folklore`:                 `https://mythology.stackexchange.com/`,
	`Network Engineering`:                  `https://networkengineering.stackexchange.com/`,
	`Open Data`:                            `https://opendata.stackexchange.com/`,
	`Open Source`:                          `https://opensource.stackexchange.com/`,
	`Operations Research`:                  `https://or.stackexchange.com/`,
	`Parenting`:                            `https://parenting.stackexchange.com/`,
	`Personal Finance & Money`:             `https://money.stackexchange.com/`,
	`Pets`:                                 `https://pets.stackexchange.com/`,
	`Philosophy`:                           `https://philosophy.stackexchange.com/`,
	`Photography`:                          `https://photo.stackexchange.com/`,
	`Physical Fitness`:                     `https://fitness.stackexchange.com/`,
	`Physics`:                              `https://physics.stackexchange.com/`,
	`Poker`:                                `https://poker.stackexchange.com/`,
	`Politics`:                             `https://politics.stackexchange.com/`,
	`Portuguese Language`:                  `https://portuguese.stackexchange.com/`,
	`Project Management`:                   `https://pm.stackexchange.com/`,
	`Psychology & Neuroscience `:           `https://psychology.stackexchange.com/`,
	`Puzzling`:                             `https://puzzling.stackexchange.com/`,
	`Quantitative Finance`:                 `https://quant.stackexchange.com/`,
	`Quantum Computing`:                    `https://quantumcomputing.stackexchange.com/`,
	`Raspberry Pi`:                         `https://raspberrypi.stackexchange.com/`,
	`Retrocomputing`:                       `https://retrocomputing.stackexchange.com/`,
	`Reverse Engineering`:                  `https://reverseengineering.stackexchange.com/`,
	`Robotics`:                             `https://robotics.stackexchange.com/`,
	`Role-playing Games`:                   `https://rpg.stackexchange.com/`,
	`Russian Language`:                     `https://russian.stackexchange.com/`,
	`Salesforce`:                           `https://salesforce.stackexchange.com/`,
	`Science Fiction & Fantasy`:            `https://scifi.stackexchange.com/`,
	`Seasoned Advice`:                      `https://cooking.stackexchange.com/`,
	`Server Fault`:                         `https://serverfault.com/`,
	`SharePoint`:                           `https://sharepoint.stackexchange.com/`,
	`Signal Processing`:                    `https://dsp.stackexchange.com/`,
	`Sitecore`:                             `https://sitecore.stackexchange.com/`,
	`Skeptics`:                             `https://skeptics.stackexchange.com/`,
	`Software Engineering`:                 `https://softwareengineering.stackexchange.com/`,
	`Software Quality Assurance & Testing`: `https://sqa.stackexchange.com/`,
	`Software Recommendations`:             `https://softwarerecs.stackexchange.com/`,
	`Sound Design`:                         `https://sound.stackexchange.com/`,
	`Space Exploration`:                    `https://space.stackexchange.com/`,
	`Spanish Language`:                     `https://spanish.stackexchange.com/`,
	`Sports`:                               `https://sports.stackexchange.com/`,
	`Stack Apps`:                           `https://stackapps.com/`,
	`Stack Overflow`:                       `https://stackoverflow.com/`,
	`Stack Overflow em Português`:          `https://pt.stackoverflow.com/`,
	`Stack Overflow en español`:            `https://es.stackoverflow.com/`,
	`Stack Overflow на русском`:            `https://ru.stackoverflow.com/`,
	`Stellar`:                              `https://stellar.stackexchange.com/`,
	`Super User`:                           `https://superuser.com`,
	`Sustainable Living`:                   `https://sustainability.stackexchange.com/`,
	`TeX - LaTeX`:                          `https://tex.stackexchange.com/`,
	`Tezos`:                                `https://tezos.stackexchange.com/`,
	`The Great Outdoors`:                   `https://outdoors.stackexchange.com/`,
	`The Workplace`:                        `https://workplace.stackexchange.com/`,
	`Theoretical Computer Science`:         `https://cstheory.stackexchange.com/`,
	`Tor`:                                  `https://tor.stackexchange.com/`,
	`Travel`:                               `https://travel.stackexchange.com/`,
	`Tridion`:                              `https://tridion.stackexchange.com/`,
	`Ukrainian Language`:                   `https://ukrainian.stackexchange.com/`,
	`Unix & Linux`:                         `https://unix.stackexchange.com/`,
	`User Experience`:                      `https://ux.stackexchange.com/`,
	`Veganism & Vegetarianism`:             `https://vegetarianism.stackexchange.com/`,
	`Vi and Vim`:                           `https://vi.stackexchange.com/`,
	`Video Production`:                     `https://video.stackexchange.com/`,
	`Web Applications`:                     `https://webapps.stackexchange.com/`,
	`Webmasters`:                           `https://webmasters.stackexchange.com/`,
	`Windows Phone`:                        `https://windowsphone.stackexchange.com/`,
	`Woodworking`:                          `https://woodworking.stackexchange.com/`,
	`WordPress Development`:                `https://wordpress.stackexchange.com/`,
	`Worldbuilding`:                        `https://worldbuilding.stackexchange.com/`,
	`Writing`:                              `https://writing.stackexchange.com/`,
	`elementary OS`:                        `https://elementaryos.stackexchange.com/`,
	`Русский язык`:                         `https://rus.stackexchange.com/`,
	`スタック・オーバーフロー`:                         `https://ja.stackoverflow.com/`,
}

// Maps the name of a Q&A site's database to a Q&A site's URL.
var dburls = map[string]string{
	"ServerFault":                      "https://serverfault.com/",
	"StackApps":                        "https://stackapps.com/",
	"StackExchange.3dprinting":         "https://3dprinting.stackexchange.com/",
	"StackExchange.Academia":           "https://academia.stackexchange.com/",
	"StackExchange.Ai":                 "https://ai.stackexchange.com/",
	"StackExchange.Android":            "https://android.stackexchange.com/",
	"StackExchange.Anime":              "https://anime.stackexchange.com/",
	"StackExchange.Apple":              "https://apple.stackexchange.com/",
	"StackExchange.Arduino":            "https://arduino.stackexchange.com/",
	"StackExchange.Astronomy":          "https://astronomy.stackexchange.com/",
	"StackExchange.Audio":              "https://video.stackexchange.com/",
	"StackExchange.Aviation":           "https://aviation.stackexchange.com/",
	"StackExchange.Beer":               "https://alcohol.stackexchange.com/",
	"StackExchange.Bicycles":           "https://bicycles.stackexchange.com/",
	"StackExchange.Bioinformatics":     "https://bioinformatics.stackexchange.com/",
	"StackExchange.Biology":            "https://biology.stackexchange.com/",
	"StackExchange.Bitcoin":            "https://bitcoin.stackexchange.com/",
	"StackExchange.Blender":            "https://blender.stackexchange.com/",
	"StackExchange.Boardgames":         "https://boardgames.stackexchange.com/",
	"StackExchange.Bricks":             "https://bricks.stackexchange.com/",
	"StackExchange.Buddhism":           "https://buddhism.stackexchange.com/",
	"StackExchange.Chemistry":          "https://chemistry.stackexchange.com/",
	"StackExchange.Chess":              "https://chess.stackexchange.com/",
	"StackExchange.Chinese":            "https://chinese.stackexchange.com/",
	"StackExchange.Christianity":       "https://christianity.stackexchange.com/",
	"StackExchange.Civicrm":            "https://civicrm.stackexchange.com/",
	"StackExchange.Codegolf":           "https://codegolf.stackexchange.com/",
	"StackExchange.Codereview":         "https://codereview.stackexchange.com/",
	"StackExchange.Coffee":             "https://coffee.stackexchange.com/",
	"StackExchange.Cogsci":             "https://psychology.stackexchange.com/",
	"StackExchange.Computergraphics":   "https://computergraphics.stackexchange.com/",
	"StackExchange.Conlang":            "https://conlang.stackexchange.com/",
	"StackExchange.Cooking":            "https://cooking.stackexchange.com/",
	"StackExchange.Craftcms":           "https://craftcms.stackexchange.com/",
	"StackExchange.Crafts":             "https://crafts.stackexchange.com/",
	"StackExchange.Crypto":             "https://crypto.stackexchange.com/",
	"StackExchange.Cs":                 "https://cs.stackexchange.com/",
	"StackExchange.Cseducators":        "https://cseducators.stackexchange.com/",
	"StackExchange.Cstheory":           "https://cstheory.stackexchange.com/",
	"StackExchange.Datascience":        "https://datascience.stackexchange.com/",
	"StackExchange.Dba":                "https://dba.stackexchange.com/",
	"StackExchange.Devops":             "https://devops.stackexchange.com/",
	"StackExchange.Diy":                "https://diy.stackexchange.com/",
	"StackExchange.Drones":             "https://drones.stackexchange.com/",
	"StackExchange.Drupal":             "https://drupal.stackexchange.com/",
	"StackExchange.Dsp":                "https://dsp.stackexchange.com/",
	"StackExchange.Earthscience":       "https://earthscience.stackexchange.com/",
	"StackExchange.Ebooks":             "https://ebooks.stackexchange.com/",
	"StackExchange.Economics":          "https://economics.stackexchange.com/",
	"StackExchange.Electronics":        "https://electronics.stackexchange.com/",
	"StackExchange.Elementaryos":       "https://elementaryos.stackexchange.com/",
	"StackExchange.Ell":                "https://ell.stackexchange.com/",
	"StackExchange.Emacs":              "https://emacs.stackexchange.com/",
	"StackExchange.Engineering":        "https://engineering.stackexchange.com/",
	"StackExchange.English":            "https://english.stackexchange.com/",
	"StackExchange.Eosio":              "https://eosio.stackexchange.com/",
	"StackExchange.Esperanto":          "https://esperanto.stackexchange.com/",
	"StackExchange.Ethereum":           "https://ethereum.stackexchange.com/",
	"StackExchange.Expatriates":        "https://expatriates.stackexchange.com/",
	"StackExchange.Expressionengine":   "https://expressionengine.stackexchange.com/",
	"StackExchange.Fitness":            "https://fitness.stackexchange.com/",
	"StackExchange.Freelancing":        "https://freelancing.stackexchange.com/",
	"StackExchange.French":             "https://french.stackexchange.com/",
	"StackExchange.GameDev":            "https://gamedev.stackexchange.com/",
	"StackExchange.Gaming":             "https://gaming.stackexchange.com/",
	"StackExchange.Garage":             "https://mechanics.stackexchange.com/",
	"StackExchange.Gardening":          "https://gardening.stackexchange.com/",
	"StackExchange.Genealogy":          "https://genealogy.stackexchange.com/",
	"StackExchange.German":             "https://german.stackexchange.com/",
	"StackExchange.Gis":                "https://gis.stackexchange.com/",
	"StackExchange.Graphicdesign":      "https://graphicdesign.stackexchange.com/",
	"StackExchange.Ham":                "https://ham.stackexchange.com/",
	"StackExchange.Hardwarerecs":       "https://hardwarerecs.stackexchange.com/",
	"StackExchange.Health":             "https://medicalsciences.stackexchange.com/",
	"StackExchange.Hermeneutics":       "https://hermeneutics.stackexchange.com/",
	"StackExchange.Hinduism":           "https://hinduism.stackexchange.com/",
	"StackExchange.History":            "https://history.stackexchange.com/",
	"StackExchange.Homebrew":           "https://homebrew.stackexchange.com/",
	"StackExchange.Hsm":                "https://hsm.stackexchange.com/",
	"StackExchange.Interpersonal":      "https://interpersonal.stackexchange.com/",
	"StackExchange.Iot":                "https://iot.stackexchange.com/",
	"StackExchange.Iota":               "https://iota.stackexchange.com/",
	"StackExchange.Islam":              "https://islam.stackexchange.com/",
	"StackExchange.Italian":            "https://italian.stackexchange.com/",
	"StackExchange.Japanese":           "https://japanese.stackexchange.com/",
	"StackExchange.Joomla":             "https://joomla.stackexchange.com/",
	"StackExchange.Judaism":            "https://judaism.stackexchange.com/",
	"StackExchange.Korean":             "https://korean.stackexchange.com/",
	"StackExchange.Languagelearning":   "https://languagelearning.stackexchange.com/",
	"StackExchange.Latin":              "https://latin.stackexchange.com/",
	"StackExchange.Law":                "https://law.stackexchange.com/",
	"StackExchange.Lifehacks":          "https://lifehacks.stackexchange.com/",
	"StackExchange.Linguistics":        "https://linguistics.stackexchange.com/",
	"StackExchange.Literature":         "https://literature.stackexchange.com/",
	"StackExchange.Magento":            "https://magento.stackexchange.com/",
	"StackExchange.Martialarts":        "https://martialarts.stackexchange.com/",
	"StackExchange.Materials":          "https://materials.stackexchange.com/",
	"StackExchange.Math":               "https://math.stackexchange.com/",
	"StackExchange.Matheducators":      "https://matheducators.stackexchange.com/",
	"StackExchange.Mathematica":        "https://mathematica.stackexchange.com/",
	"StackExchange.Mathoverflow":       "https://mathoverflow.net/",
	"StackExchange.Meta":               "https://meta.stackexchange.com/",
	"StackExchange.Moderators":         "https://communitybuilding.stackexchange.com/",
	"StackExchange.Monero":             "https://monero.stackexchange.com/",
	"StackExchange.Money":              "https://money.stackexchange.com/",
	"StackExchange.Movies":             "https://movies.stackexchange.com/",
	"StackExchange.Music":              "https://music.stackexchange.com/",
	"StackExchange.Musicfans":          "https://musicfans.stackexchange.com/",
	"StackExchange.Mythology":          "https://mythology.stackexchange.com/",
	"StackExchange.Networkengineering": "https://networkengineering.stackexchange.com/",
	"StackExchange.Opendata":           "https://opendata.stackexchange.com/",
	"StackExchange.Opensource":         "https://opensource.stackexchange.com/",
	"StackExchange.Or":                 "https://or.stackexchange.com/",
	"StackExchange.Outdoors":           "https://outdoors.stackexchange.com/",
	"StackExchange.Parenting":          "https://parenting.stackexchange.com/",
	"StackExchange.Patents":            "https://patents.stackexchange.com/",
	"StackExchange.Pets":               "https://pets.stackexchange.com/",
	"StackExchange.Philosophy":         "https://philosophy.stackexchange.com/",
	"StackExchange.Photography":        "https://photo.stackexchange.com/",
	"StackExchange.Physics":            "https://physics.stackexchange.com/",
	"StackExchange.Pm":                 "https://pm.stackexchange.com/",
	"StackExchange.Poker":              "https://poker.stackexchange.com/",
	"StackExchange.Politics":           "https://politics.stackexchange.com/",
	"StackExchange.Portuguese":         "https://portuguese.stackexchange.com/",
	"StackExchange.Programmers":        "https://softwareengineering.stackexchange.com/",
	"StackExchange.Puzzling":           "https://puzzling.stackexchange.com/",
	"StackExchange.Quant":              "https://quant.stackexchange.com/",
	"StackExchange.Quantumcomputing":   "https://quantumcomputing.stackexchange.com/",
	"StackExchange.Raspberrypi":        "https://raspberrypi.stackexchange.com/",
	"StackExchange.Retrocomputing":     "https://retrocomputing.stackexchange.com/",
	"StackExchange.Reverseengineering": "https://reverseengineering.stackexchange.com/",
	"StackExchange.Robotics":           "https://robotics.stackexchange.com/",
	"StackExchange.Rpg":                "https://rpg.stackexchange.com/",
	"StackExchange.Rus":                "https://rus.stackexchange.com/",
	"StackExchange.Russian":            "https://russian.stackexchange.com/",
	"StackExchange.Salesforce":         "https://salesforce.stackexchange.com/",
	"StackExchange.Scicomp":            "https://scicomp.stackexchange.com/",
	"StackExchange.Scifi":              "https://scifi.stackexchange.com/",
	"StackExchange.Security":           "https://security.stackexchange.com/",
	"StackExchange.Sharepoint":         "https://sharepoint.stackexchange.com/",
	"StackExchange.Sitecore":           "https://sitecore.stackexchange.com/",
	"StackExchange.Skeptics":           "https://skeptics.stackexchange.com/",
	"StackExchange.Softwarerecs":       "https://softwarerecs.stackexchange.com/",
	"StackExchange.Sound":              "https://sound.stackexchange.com/",
	"StackExchange.Space":              "https://space.stackexchange.com/",
	"StackExchange.Spanish":            "https://spanish.stackexchange.com/",
	"StackExchange.Sports":             "https://sports.stackexchange.com/",
	"StackExchange.Sqa":                "https://sqa.stackexchange.com/",
	"StackExchange.Stats":              "https://stats.stackexchange.com/",
	"StackExchange.Stellar":            "https://stellar.stackexchange.com/",
	"StackExchange.Sustainability":     "https://sustainability.stackexchange.com/",
	"StackExchange.Tex":                "https://tex.stackexchange.com/",
	"StackExchange.Tezos":              "https://tezos.stackexchange.com/",
	"StackExchange.Tor":                "https://tor.stackexchange.com/",
	"StackExchange.Travel":             "https://travel.stackexchange.com/",
	"StackExchange.Tridion":            "https://tridion.stackexchange.com/",
	"StackExchange.Ubuntu":             "https://askubuntu.com/",
	"StackExchange.Ukrainian":          "https://ukrainian.stackexchange.com/",
	"StackExchange.Unix":               "https://unix.stackexchange.com/",
	"StackExchange.Ux":                 "https://ux.stackexchange.com/",
	"StackExchange.Vegetarian":         "https://vegetarianism.stackexchange.com/",
	"StackExchange.Vi":                 "https://vi.stackexchange.com/",
	"StackExchange.WebApps":            "https://webapps.stackexchange.com/",
	"StackExchange.Webmasters":         "https://webmasters.stackexchange.com/",
	"StackExchange.Windowsphone":       "https://windowsphone.stackexchange.com/",
	"StackExchange.Woodworking":        "https://woodworking.stackexchange.com/",
	"StackExchange.Wordpress":          "https://wordpress.stackexchange.com/",
	"StackExchange.Workplace":          "https://workplace.stackexchange.com/",
	"StackExchange.Worldbuilding":      "https://worldbuilding.stackexchange.com/",
	"StackExchange.Writers":            "https://writing.stackexchange.com/",
	"StackOverflow":                    "https://stackoverflow.com/",
	"StackOverflow.Br":                 "https://pt.stackoverflow.com/",
	"StackOverflow.Es":                 "https://es.stackoverflow.com/",
	"StackOverflow.Ja":                 "https://ja.stackoverflow.com/",
	"StackOverflow.Ru":                 "https://ru.stackoverflow.com/",
	"SuperUser":                        "https://superuser.com/",
}
