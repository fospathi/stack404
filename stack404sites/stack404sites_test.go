package stack404sites_test

import (
	"testing"

	. "gitlab.com/fospathi/stack404/stack404sites"
)

func Test_Contains(t *testing.T) {
	for _, u := range []string{
		"stackoverflow.com",
		"https://android.stackexchange.com/",
		"www.superuser.com",
		"https://www.superuser.com",
	} {
		got, err := Contains(u)
		if err != nil {
			t.Errorf("got err %v; for %s\n", err, u)
		}
		if !got {
			t.Errorf("got false; want true; for %s\n", u)
		}
	}

	for _, u := range []string{
		"icecream.stackoverflow.com",
		"https://icecream.stackexchange.com/",
		"https://icecream.android.stackexchange.com/",
		"www.stackexchange.com", // Not an actual Q&A site itself.
	} {
		got, err := Contains(u)
		if err != nil {
			t.Errorf("got err %v; for %s\n", err, u)
		}
		if got {
			t.Errorf("got true; want false; for %s\n", u)
		}
	}

	_, err := Contains("www.stackexchange.com\t") // control characters not allowed in URLS.
	if err == nil {
		t.Errorf("got nil want error")
	}

	_, err = Contains("abc.") // Malformed hostname.
	if err == nil {
		t.Errorf("got nil want error")
	}

	_, err = Contains("www.") // Malformed hostname.
	if err == nil {
		t.Errorf("got nil want error")
	}
}

func Test_IsExternal(t *testing.T) {
	for _, u := range []string{
		"stackoverflow.com",
		"https://android.stackexchange.com/",
		"www.superuser.com",
		"https://www.superuser.com",
	} {
		got, err := IsExternal(u)
		if err != nil {
			t.Errorf("got err %v; for %s\n", err, u)
		}
		if got {
			t.Errorf("got true; want false; for %s\n", u)
		}
	}

	for _, u := range []string{
		"icecream.com",
		"https://lotsof.icecream.com/",
		"www.icecream.com",
		"https://www.icecream.com",
	} {
		got, err := IsExternal(u)
		if err != nil {
			t.Errorf("got err %v; for %s\n", err, u)
		}
		if !got {
			t.Errorf("got false; want true; for %s\n", u)
		}
	}

	_, err := IsExternal("www.stackexchange.com\t") // control characters not allowed in URLS.
	if err == nil {
		t.Errorf("got nil want error")
	}

	_, err = IsExternal("http://www.") // Malformed hostname.
	if err == nil {
		t.Errorf("got nil want error")
	}
}

func Test_URL(t *testing.T) {
	got, ok := URL(`Arqade`)
	want := `https://gaming.stackexchange.com/`
	if !ok || want != got {
		t.Errorf("got %v; want %v\n", got, want)
	}

	_, ok = URL(`Arqades`)
	if ok {
		t.Errorf("got true; want false")
	}
}
