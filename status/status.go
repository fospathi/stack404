/*
Package status provides primitives for checking the status of websites.
*/
package status

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sync"
	"time"

	"gitlab.com/fospathi/stack404/config"
	"gitlab.com/fospathi/stack404/firecrow"
	"gitlab.com/fospathi/stack404/link"
)

// Use a single screenshot function to run screenshot making browsers in parallel.
var screenshot, _ = firecrow.New(config.PicWidth, config.PicHeight)

// OfSites gets the statuses of the resources pointed to by the argument links and publishes them to the
// argument publisher.
func OfSites(pub func(link.Status), vl ...link.L) error {
	// Implement a bounded parallelism pipeline to run network and screenshot operations in parallel. See
	// https://blog.golang.org/pipelines/bounded.go for an example of a bounded parallelism pipeline.

	// The number of digesters should equal, or at least not exceed, the number of screenshot making browsers
	// running in parallel.
	const numDigesters = 2

	produceLinks := func(done <-chan struct{}) (<-chan link.L, <-chan error) {
		lc := make(chan link.L)
		errc := make(chan error, 1)
		go func() {
			defer close(lc)
			for _, l := range vl {
				select {
				case lc <- l:
				case <-done:
					errc <- errors.New("link producer cancelled")
					return
				}
			}
			errc <- nil
		}()
		return lc, errc
	}

	type status struct {
		ls  link.Status
		err error
	}

	digester := func(done <-chan struct{}, lc <-chan link.L, sc chan<- status) {
		for l := range lc {
			ls, err := OfSite(l)
			select {
			case sc <- status{ls, err}:
			case <-done:
				return
			}
		}
	}

	statusesAll := func() error {
		done := make(chan struct{})
		defer close(done)

		lc, errc := produceLinks(done)

		sc := make(chan status)
		var wg sync.WaitGroup
		wg.Add(numDigesters)
		for i := 0; i < numDigesters; i++ {
			go func() {
				digester(done, lc, sc)
				wg.Done()
			}()
		}
		go func() {
			wg.Wait()
			close(sc)
		}()

		var (
			firecrowError   *firecrow.Error
			statusCodeError *Error
		)
		for s := range sc {
			if s.err != nil {
				switch {
				case errors.As(s.err, &firecrowError):
					fmt.Printf("\n%v\n%v\n", s.err, s.ls)
				case errors.As(s.err, &statusCodeError):
					fmt.Printf("\n%v\n%v\n", s.err, s.ls)
				default:
					return s.err
				}
				continue
			}
			pub(s.ls)
		}
		if err := <-errc; err != nil {
			return err
		}
		return nil
	}

	return statusesAll()
}

// OfSite gets the status of the resource pointed to by the argument link.
func OfSite(l link.L) (link.Status, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(config.Timeout)*time.Second)
	defer cancel()

	errc := make(chan error, 2)
	picc := make(chan firecrow.Screenshot, 1)
	cc := make(chan code, 1)

	go func() {
		if pic, err := screenshot(ctx, l); err != nil {
			errc <- err
		} else {
			picc <- pic
		}
	}()

	go func() {
		if c, err := statusCode(ctx, l); err != nil {
			errc <- err
		} else {
			cc <- c
		}
	}()

	{
		var (
			c                         code
			p                         firecrow.Screenshot
			codeComplete, picComplete bool
		)
		for {
			select {
			case err := <-errc:
				return newLinkStatus(l, c, p), err
			case c = <-cc:
				codeComplete = true
			case p = <-picc:
				picComplete = true
			}
			if codeComplete && picComplete {
				return newLinkStatus(l, c, p), nil
			}
		}
	}
}

func newLinkStatus(l link.L, code code, pic firecrow.Screenshot) link.Status {
	return link.Status{
		SELink: link.SELink{
			Relative: l.Ancestor(),
			Target:   l.URL(),
			Text:     l.String(),
		},
		ResponseStatusCode: code.code,
		CodeOk:             code.ok,
		CodeTimeout:        code.timeout,
		Screenshot:         pic.Bytes,
		ScreenshotOk:       pic.Ok,
		ScreenshotTimeout:  pic.Timeout,
	}
}

// A HTTP status response code.
type code struct {
	code    int
	ok      bool // False if failed to obtain a code.
	timeout bool // True if timed out before getting a code.
}

// Return the response status code of a request made to the resource pointed to by the argument link.
func statusCode(ctx context.Context, l link.L) (code, error) {
	// Use a client that doesn't follow redirects.
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	req, err := http.NewRequestWithContext(ctx, "HEAD", link.Schemify(l.URL()), nil)
	if err != nil {
		return code{}, wrap(err, "http.NewRequestWithContext", 0)
	}
	resp, err := client.Do(req)
	if err != nil {
		// Let these errors pass so the user can check these sites.
		e := err.(*url.Error)
		return code{timeout: e.Timeout()}, nil
	}
	defer resp.Body.Close()
	_, _ = ioutil.ReadAll(resp.Body)
	return code{code: resp.StatusCode, ok: true, timeout: false}, nil
}

// Wrap the argument non-nil error in a statusCode error.
func wrap(err error, info string, code int) *Error {
	if len(info) > 0 {
		return &Error{fmt.Errorf("%v: %w", info, err)}
	}
	return &Error{err}
}

// An Error indicates that something happened when trying to get the response status code of a site to a HEAD
// request.
type Error struct {
	err error
}

func (e *Error) Error() string {
	return fmt.Sprintf("status code error: %v", e.err)
}

func (e *Error) Unwrap() error {
	return e.err
}
