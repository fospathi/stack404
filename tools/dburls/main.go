/*
A tool to generate a map that maps a StackExchange database name to the URL of the StackExchange Q&A site
corresponding to that database.
*/
package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

const queryResultsPath = "dburls.csv"

func main() {
	f, err := os.Open(queryResultsPath)
	fatal(err)

	r := csv.NewReader(f)

	// Skip over the CSV file's column titles.
	_, err = r.Read()
	fatal(err)

	var b strings.Builder
	b.WriteString("var dburls = map[string]string{\n")
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		fatal(err)
		fmt.Fprintf(&b, "\t\"%v\": \"%v\",\n", record[0], record[1])
	}
	b.WriteString("}\n")
	fmt.Printf("%v", b.String())
}

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
